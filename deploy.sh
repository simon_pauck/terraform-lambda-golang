#!/bin/bash

set -e

cd "$(dirname "$(dirname "$0")")"

rm -rf ./bin

echo "Build..."

cd src/hello
go test ./...
env GOOS=linux GOARCH=amd64 go build -o ../../bin/hello
cd ../..

echo "Deploy..."
cd infrastructure
terraform init -input=false
# terraform apply -input=false -auto-approve
terraform apply
cd ../

echo -e "\nDeployment done\n"
