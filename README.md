# Prototype AWS Lambda

- Lambda running Golang
- Deployed with Terraform

Based on:

- <https://adamtheautomator.com/terraform-aws-lambda/>
- <https://levelup.gitconnected.com/setup-your-go-lambda-and-deploy-with-terraform-9105bda2bd18>
- extra: <https://www.softkraft.co/aws-lambda-in-golang/>
